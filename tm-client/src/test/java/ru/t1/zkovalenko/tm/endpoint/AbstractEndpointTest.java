package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IAuthEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IDomainEndpoint;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.request.data.DataBackupLoadRequest;
import ru.t1.zkovalenko.tm.dto.request.data.DataBackupSaveRequest;
import ru.t1.zkovalenko.tm.dto.request.data.DataDisableAutoBackupRequest;
import ru.t1.zkovalenko.tm.dto.request.data.DataEnableAutoBackupRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLogoutRequest;
import ru.t1.zkovalenko.tm.service.PropertyService;

public abstract class AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    public void loadBackup(final String userToken) {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(userToken);
        domainEndpoint.backupLoadData(request);
    }

    public void saveBackup(final String userToken) {
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest(userToken);
        domainEndpoint.backupSaveData(request);
    }

    public String login(@NotNull final String login, @NotNull final String password) {
        UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    public void logout(final String userToken) {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(userToken);
        request.setToken(userToken);
        authEndpoint.logout(request);
    }

    public void serverAutoBackup(final boolean enable, final String userToken) {
        if (enable) {
            @NotNull final DataEnableAutoBackupRequest requestEnable = new DataEnableAutoBackupRequest(userToken);
            domainEndpoint.enableAutoBackupData(requestEnable);
        } else {
            @NotNull final DataDisableAutoBackupRequest requestDisable = new DataDisableAutoBackupRequest(userToken);
            domainEndpoint.disableAutoBackupData(requestDisable);
        }
    }

}
