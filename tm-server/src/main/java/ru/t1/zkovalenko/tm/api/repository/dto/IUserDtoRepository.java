package ru.t1.zkovalenko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.enumerated.Role;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @NotNull UserDTO create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password
    );

    @NotNull UserDTO create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password,
            @NotNull String email
    );

    @NotNull UserDTO create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    );

    @NotNull UserDTO create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password,
            @Nullable String email,
            @Nullable Role role
    );

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull Boolean isEmailExist(@NotNull String email);
}
