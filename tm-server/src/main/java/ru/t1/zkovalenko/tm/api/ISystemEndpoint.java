package ru.t1.zkovalenko.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.system.ServerAboutRequest;
import ru.t1.zkovalenko.tm.dto.request.system.ServerVersionRequest;
import ru.t1.zkovalenko.tm.dto.response.system.ServerAboutResponse;
import ru.t1.zkovalenko.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
