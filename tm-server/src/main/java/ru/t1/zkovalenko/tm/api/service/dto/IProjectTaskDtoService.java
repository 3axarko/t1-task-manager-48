package ru.t1.zkovalenko.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.zkovalenko.tm.api.repository.dto.ITaskDtoRepository;

import javax.persistence.EntityManager;

public interface IProjectTaskDtoService {

    @NotNull ITaskDtoRepository getTaskRepository(@NotNull EntityManager entityManager);

    @NotNull IProjectDtoRepository getProjectRepository(@NotNull EntityManager entityManager);

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
