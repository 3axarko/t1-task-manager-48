package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDtoService getSessionService();

    void setDisableAutoBackup(boolean disableAutoBackup);

}
