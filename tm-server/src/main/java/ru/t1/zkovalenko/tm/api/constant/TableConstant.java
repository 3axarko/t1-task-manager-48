package ru.t1.zkovalenko.tm.api.constant;

import org.jetbrains.annotations.NotNull;

public interface TableConstant {

    @NotNull
    String ENTITY_USER = "User";

    @NotNull
    String ENTITY_PROJECT = "Project";

    @NotNull
    String ENTITY_TASK = "Task";

    @NotNull
    String ENTITY_SESSION = "Session";

    @NotNull
    String ENTITY_USER_DTO = "UserDTO";

    @NotNull
    String ENTITY_PROJECT_DTO = "ProjectDTO";

    @NotNull
    String ENTITY_TASK_DTO = "TaskDTO";

    @NotNull
    String ENTITY_SESSION_DTO = "SessionDTO";

    @NotNull
    String TABLE_USER = "tm_user";

    @NotNull
    String TABLE_PROJECT = "tm_project";

    @NotNull
    String TABLE_TASK = "tm_task";

    @NotNull
    String TABLE_SESSION = "tm_session";

    @NotNull
    String FIELD_ID = "id";

    @NotNull
    String FIELD_CREATED = "created";

    @NotNull
    String FIELD_DESCRIPTION = "description";

    @NotNull
    String FIELD_EMAIL = "email";

    @NotNull
    String FIELD_FIRST_NAME = "first_name";

    @NotNull
    String FIELD_LAST_NAME = "last_name";

    @NotNull
    String FIELD_MIDDLE_NAME = "middle_name";

    @NotNull
    String FIELD_LOCKED_FLG = "locked_flg";

    @NotNull
    String FIELD_LOGIN = "login";

    @NotNull
    String FIELD_NAME = "name";

    @NotNull
    String FIELD_PASSWORD = "password";

    @NotNull
    String FIELD_PROJECT_ID = "project_id";

    @NotNull
    String FIELD_ROLE = "role";

    @NotNull
    String FIELD_STATUS = "status";

    @NotNull
    String FIELD_USER_ID = "user_id";

}
