package ru.t1.zkovalenko.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "123321";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "111222";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    public static final String DB_USER = "database.username";

    @NotNull
    public static final String DB_USER_DEFAULT = "admin";

    @NotNull
    public static final String DB_PASSWORD = "database.password";

    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "admin";

    @NotNull
    public static final String DB_URL = "database.url";

    @NotNull
    public static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";

    @NotNull
    public static final String DB_DRIVER = "database.driver";

    @NotNull
    public static final String DB_DIALECT = "database.dialect";

    @NotNull
    public static final String DB_HIBER_STRATEGY = "database.hyber.strategy";

    @NotNull
    public static final String DB_HIBER_SHOWSQL = "database.hyber.showsql";

    @NotNull
    public static final String USE_SECOND_LEVEL_CACHE = "database.use_second_level_cache";

    @NotNull
    public static final String CACHE_PROVIDER_CONFIG = "database.cache_provider_config";

    @NotNull
    public static final String CACHE_REGION_FACTORY = "database.cache_region_factory";

    @NotNull
    public static final String USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    public static final String USE_MINIMAL_PUTS = "database.use_minimal_puts";

    @NotNull
    public static final String CACHE_REGION_PREFIX = "database.cache_region_prefix";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DB_URL, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public int getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, "");
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @Override
    @NotNull
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER);
    }

    @Override
    @NotNull
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT);
    }

    @Override
    @NotNull
    public String getDatabaseStrategy() {
        return getStringValue(DB_HIBER_STRATEGY);
    }

    @Override
    @NotNull
    public String getDatabaseShowSql() {
        return getStringValue(DB_HIBER_SHOWSQL);
    }


    @Override
    @NotNull
    public String getUseSecondLevelCache() {
        return getStringValue(USE_SECOND_LEVEL_CACHE);
    }

    @Override
    @NotNull
    public String getCacheProviderConfig() {
        return getStringValue(CACHE_PROVIDER_CONFIG);
    }

    @Override
    @NotNull
    public String getCacheRegionFactory() {
        return getStringValue(CACHE_REGION_FACTORY);
    }

    @Override
    @NotNull
    public String getUseQueryCache() {
        return getStringValue(USE_QUERY_CACHE);
    }

    @Override
    @NotNull
    public String getUseMinimalPuts() {
        return getStringValue(USE_MINIMAL_PUTS);
    }

    @Override
    @NotNull
    public String getCacheRegionPrefix() {
        return getStringValue(CACHE_REGION_PREFIX);
    }

}
