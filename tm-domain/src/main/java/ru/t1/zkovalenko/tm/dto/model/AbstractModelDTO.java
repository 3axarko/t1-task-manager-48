package ru.t1.zkovalenko.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
