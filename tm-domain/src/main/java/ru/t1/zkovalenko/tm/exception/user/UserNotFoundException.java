package ru.t1.zkovalenko.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("User not Found");
    }

}
