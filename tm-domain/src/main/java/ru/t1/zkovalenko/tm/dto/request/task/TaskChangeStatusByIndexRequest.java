package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;
import ru.t1.zkovalenko.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
