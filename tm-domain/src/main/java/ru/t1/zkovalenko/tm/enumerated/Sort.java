package ru.t1.zkovalenko.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.comparator.CreatedComparator;
import ru.t1.zkovalenko.tm.comparator.NameComparator;
import ru.t1.zkovalenko.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by Name", NameComparator.INSTANCE),
    BY_STATUS("Sort by Status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by Created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    private final Comparator comparator;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
